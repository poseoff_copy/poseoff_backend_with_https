from fastapi import APIRouter, status, Depends, HTTPException, Form
from database.controller.schemas import PoseoffDetails, SettingDetails
from database.controller.Database import get_db
from database.model.models import PoseOff, Settings
from sqlalchemy.orm import Session
from os.path import isfile, join
from config import Pathconfig, User
from base64 import b64decode
from typing import Union
from io import BytesIO
from PIL import Image
import uuid
import os




router = APIRouter(
    prefix='/utils',
    tags=['Utils'],
    responses={404: {"description": "Not found"}}
    )



@router.post("/palyer-name")
def get_player_name(db: Session = Depends(get_db)):
    """get player name to show"""
    get_player = []
    player = db.query(PoseoffDetails).count()
    if not player:
        user_id = User.count
        username = User.first_user
        get_player.append({"_id": user_id, "username": username})
    else:
        data = db.query(PoseoffDetails).order_by(PoseoffDetails._id.desc()).first()
        user_id = data._id + User.count
        username = User.user + str(user_id)
        get_player.append({"_id": user_id, "username": username})
    return get_player




@router.post("/save-scores")
def save_user_scores(request: PoseOff, db: Session = Depends(get_db)):
    """save all user's scores after finishing of all poses"""
    try:
        poses = [request.p1_score, request.p2_score, request.p3_score, request.p4_score, request.p5_score, request.p6_score, request.p7_score, request.p8_score, request.p9_score, request.p10_score]
        pose_counts = (10 - poses.count(""))
        total_score = sum(int(value) for value in poses if value)
        if request.username:
            player_name = request.username
        elif not db.query(PoseoffDetails).count():
            player_name = "Anonymous1"
        else:
            data = db.query(PoseoffDetails).order_by(PoseoffDetails._id.desc()).first()
            player_name = f"Anonymous{data._id + 1}"

        score_data = PoseoffDetails(
                                  username=player_name,
                                  platform=request.platform,
                                  pose_count=pose_counts,
                                  p1_score=request.p1_score,
                                  p2_score=request.p2_score,
                                  p3_score=request.p3_score,
                                  p4_score=request.p4_score,
                                  p5_score=request.p5_score,
                                  p6_score=request.p6_score,
                                  p7_score=request.p7_score,
                                  p8_score=request.p8_score,
                                  p9_score=request.p9_score,
                                  p10_score=request.p10_score,
                                  final_score=total_score,
                                  img_link="none"
                                  )
        db.add(score_data)
        db.commit()

        data = db.query(PoseoffDetails).order_by(PoseoffDetails._id.desc())
        player_scores = data[0]
    except Exception as e:
        return {"Error": str(e)}
    return {"status": "success", "player_scores": player_scores}




@router.get("/get-scores")
def get_user_scores(tx_id: str, db: Session = Depends(get_db)):
    """get all user's scores after finishing of all poses"""
    score_list = []
    position = []
    try:
        data = db.query(PoseoffDetails).filter(PoseoffDetails._id == tx_id).all()
        if not data:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
        
        get_final_scores = db.query(PoseoffDetails).order_by(PoseoffDetails.pose_count.desc(), PoseoffDetails.final_score.asc()).all()
        for res in get_final_scores:
            position.append({"_id": res._id, "username": res.username, "final_score": res.final_score})
            index = next(((index+1) for index, user in enumerate(position) if user["_id"] == int(tx_id)), None)

        for result in data:
            score_list.append({"_id": result._id,
                               "username": result.username,
                                "pose_1": result.p1_score,
                                "pose_2": result.p2_score,
                                "pose_3": result.p3_score,
                                "pose_4": result.p4_score,
                                "pose_5": result.p5_score,
                                "pose_6": result.p6_score,
                                "pose_7": result.p7_score,
                                "pose_8": result.p8_score,
                                "pose_9": result.p9_score,
                                "pose_10": result.p10_score,
                                "final_score": result.final_score,
                                "platform": result.platform,
                                "your_position": index
                                })
    except Exception as e:
        return {"Error": str(e)}
    return score_list




@router.get("/pose-off-leaderboard")
def poseoff_leaderboard(tx_id: str, db: Session = Depends(get_db)):
    """get all user's data for leaderboard"""

    sorted_data = []
    data = db.query(PoseoffDetails).order_by(PoseoffDetails.pose_count.desc(), PoseoffDetails.final_score.asc()).all()
    if not data:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    for result in data:
        sorted_data.append({"_id": result._id,
                            "pose_count": result.pose_count,
                            "username": result.username,
                            "pose_1": result.p1_score,
                            "pose_2": result.p2_score,
                            "pose_3": result.p3_score,
                            "pose_4": result.p4_score,
                            "pose_5": result.p5_score,
                            "pose_6": result.p6_score,
                            "pose_7": result.p7_score,
                            "pose_8": result.p8_score,
                            "pose_9": result.p9_score,
                            "pose_10": result.p10_score,
                            "final_score": result.final_score,
                            "platform": result.platform
                            })
    last_score=0
    last_rank=1
    
    for i in range(len(sorted_data)):
        if last_score == sorted_data[i]["final_score"]:
            sorted_data[i]["rank"]=last_rank
        else:
            sorted_data[i]["rank"]=i+1
            last_score=sorted_data[i]["final_score"]
            last_rank=i+1
        if (sorted_data[i]["_id"])==int(tx_id):
            sorted_data[i]["current_user_rank"] = True
        else:   
            sorted_data[i]["current_user_rank"]  = False
    return sorted_data




@router.post("/save-pictures")
def save_pictures(filedata: str = Form(...), tx_id: str = Form(...)):
    """
    Here we will save the frame or pictures of user while playing game, when pose will be detected successfully.
    """
    input_path = os.path.join(Pathconfig.BasePath, tx_id)
    if not os.path.isdir(input_path):
        os.makedirs(input_path)
    
    filename = str(uuid.uuid4()) + ".png"
    upload_path = os.path.join(Pathconfig.BasePath, tx_id, filename)
    im = Image.open(BytesIO(b64decode(filedata.split(",")[1])))
    im.save(upload_path)
    path = os.path.join(Pathconfig.BasePath, tx_id)

    return {"status": "success", "_id": tx_id, "upload_path": path}



@router.get("/show-pictures")
def show_pictures(tx_id: Union[str, None] = None):
    """Get pictures based on the conditions
        return all pictures if will not enter id
        :param _id: return all the pictures of given id
    """
    img_list = []
    if not tx_id:
        for path, subdir, files in os.walk(Pathconfig.BasePath):
            for name in files:
                img_path = os.path.join(path, name)
                img_list.append({"img_link": img_path})
    else:
        file_path = os.path.join(Pathconfig.BasePath, tx_id)
        for path, subdir, files in os.walk(file_path):
            for name in files:
                img_path = os.path.join(path, name)
                img_list.append({"img_link": img_path})
    
    img_list = sorted(img_list, key=lambda x: x["img_link"])
    return img_list




@router.get("/get-players")
def get_players(db: Session = Depends(get_db)):
    players_list = []
    players = db.query(PoseoffDetails).all()
    if not players:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Player not found')
    for result in players:
        players_list.append({"_id": result._id,"username": result.username})

    return players_list



@router.post("/change-settings")
def change_settings(request: Settings, db: Session = Depends(get_db)):
    if not db.query(SettingDetails).count():
        data = SettingDetails(game_mode=request.game_mode, cam_mode=request.cam_mode, cam_url=request.cam_url)
        db.add(data)
        db.commit()
    else:
        data = db.query(SettingDetails).update({SettingDetails.game_mode: request.game_mode,
                                                SettingDetails.cam_mode: request.cam_mode,
                                                SettingDetails.cam_url: request.cam_url,
                                                })
        db.commit()
    resp = db.query(SettingDetails).first()
    return {"game_mode": resp.game_mode, "cam_mode": resp.cam_mode, "cam_url": resp.cam_url}



@router.get("/get-settings")
def get_settings(db: Session = Depends(get_db)):
    resp = db.query(SettingDetails).first()
    if not resp:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Data not found")
    return [{"game_mode": resp.game_mode, "cam_mode": resp.cam_mode, "cam_url": resp.cam_url}]