import uvicorn
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from routes.api import router as api_router
from fastapi.staticfiles import StaticFiles
from config import Pathconfig
from pathlib import Path
import ssl


app = FastAPI()

Path(Pathconfig.BasePath).mkdir(exist_ok=True)
app.mount(Pathconfig.mount_point, StaticFiles(directory=Pathconfig.BasePath),name=Pathconfig.BasePath)

origin = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origin,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)


app.include_router(api_router)

if __name__=="__main__":
    uvicorn.run("main:app", host='0.0.0.0', port=8082, log_level="info", reload=True)
