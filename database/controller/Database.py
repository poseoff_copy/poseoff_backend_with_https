from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker, DeclarativeBase
from config import Database
import os

sqlalchemy_database_url = Database.db_url
engine = create_engine(sqlalchemy_database_url, pool_size=10, max_overflow=20, echo=True)

if not database_exists(engine.url):
    create_database(engine.url)

SessionLocal = sessionmaker(autocommit=False,autoflush=False, bind=engine)

class Base(DeclarativeBase):
    pass

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

   
